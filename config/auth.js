module.exports = {
    ensureAuthenticated: function(req,res,next){ 
        if(req.isAuthenticated()){ // თუ რექუესთი ავტორიზირებულია მაგ შემთხვევაში დაუშვებს ამ ფუნქციას
            return next();
        }
        req.flash('error_msg', 'Please Log in to view this resource'); // თუ არ არის ავტორიზებული მაშინ ერორ მესიჯს გადმოსცემს და დააბრუნებს ლოგინ ფეიჯზე.
        res.redirect('/user/login');
    }
}