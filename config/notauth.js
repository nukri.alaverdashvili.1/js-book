module.exports = {
    ensureNotAuthenticated: function(req,res,next){ 
        if(!req.isAuthenticated()){ // თუ რექუესთი ავტორიზირებულია მაგ შემთხვევაში დაუშვებს ამ ფუნქციას
            return next();
        }
        req.flash('error_msg', 'You are logged in'); // თუ არ არის ავტორიზებული მაშინ ერორ მესიჯს გადმოსცემს და დააბრუნებს ლოგინ ფეიჯზე.
        res.redirect('/');
    }
}