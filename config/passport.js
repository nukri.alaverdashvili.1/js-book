const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')

// Load User Model
const User = require('../models/user')

module.exports = function(passport){
    passport.use(
        new LocalStrategy({usernameField:'email'},(email, password,done)=>{
            //Match User
            User.findOne({email:email})
            .then(user=> {
                if(!user){
                    return done(null, false, {message:'That email is not registered'}); //null იწერება ერორის ნაცვლად ანუ როდესაც ერორი არ გვაქვს, მეორე იუზერია ფალსი რადგან ვერ იპოვა ამ მეილზე იუზერი და მესიჯი რომ არ არის რეგისტრირებული
                }

                //Match password
                bcrypt.compare(password, user.password, (err, isMatch)=>{ // ვადარებთ პაროლებს, პირველი არის მომხმარებლის შეყვანილი პაროლი, ხოლო მეორე თვითონ იუზერის პაროლი და გვაქვს ფუნქცია isMatch თუ ემთხვევა პაროლები ერთმანეთს
                    if(err)throw err;
                    if(isMatch)
                    {
                        return done(null, user);
                    }else{
                        return done(null, false, {message: 'Password incorrect'})
                    }
                });
            })
            .catch(err=>console.log(err))
        })
    );
    // იუზერის ბრაუზერში ინახავს იმ მონაცემებს რომელიც შეიყვანა მომხმარებელმა
    passport.serializeUser(function(user, done) {
        done(null, user.id);
      });
      
      passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
          done(err, user);
        });
      });
}