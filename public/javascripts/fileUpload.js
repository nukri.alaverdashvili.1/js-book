FilePond.registerPlugin(
    FilePondPluginImagePreview,
    FilePondPluginImageResize,
    FilePondPluginFileEncode,
)

FilePond.setOptions({
    stylePanelAspectRatio: 50/150, // როგორი ფორმატით გამოიტანოს გამოსახულება
    imageResizeTargetWidth: 100,  //რა ზომის ფოტო შეინახოს ამისთვის ვუწერთ მონაცემებს
    imageResizeTargetHeight: 150
})
FilePond.parse(document.body);
