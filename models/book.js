const mongoose = require('mongoose') //შემოგვაქვს მონგო 
const path = require('path')

const bookSchema = new mongoose.Schema({ //ვქმნით სქემას თუ რა უნდა ჩაიწეროს ამ თეიბლში, იქნება name რომელიც იქნება სტრინგი და აუცილებლად ჩასაწერი ველი ექნება
    title:{
        type: String, 
        required:true
    },
    description:{
        type: String, 
    },
    publishDate:{
        type: Date, 
        required:true
    },
    pageCount:{
        type: Number, 
        required:true
    },
    createdAt:{
        type: Date, 
        required:true,
        default: Date.now
    },
    coverImage:{
        type: Buffer,
        required:true,
    },
    coverImageType:{
        type: String,
        required:true,
    },
    author: {
        type: mongoose.Schema.Types.ObjectId, 
        required: true,
        ref: 'Author' //სახელი აუცილებლად უნდა ემთხვეოდეს ავტორის მოდელში გაწერილ სახელს
    }
})

bookSchema.virtual('coverImagePath').get(function(){  // იმისთვის რომ იმიჯ ფასი შეიცვალოს და შიგნით ჩაიწეროს ის პასი რომელიც ბაზაში იქნება და ამით შემდეგ გამოვაჩინოთ ჰტმლ ში
    if(this.coverImage !=null && this.coverImageType != null)
    {
        return `data:${this.coverImageType}; charset=utf8; base64, ${this.coverImage.toString('base64')}`
    }
})

module.exports = mongoose.model('Book', bookSchema) //შემდეგ ვაექსპორტებთ, ანუ ჩვენს თეიბლს ექნება სახელი Author ხოლო მეორე პარამეტრი იქნება ზემოთ აღწერილი სკიმა
