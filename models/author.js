const mongoose = require('mongoose') //შემოგვაქვს მონგო 
const Book = require('./book')

const authorSchema = new mongoose.Schema({ //ვქმნით სქემას თუ რა უნდა ჩაიწეროს ამ თეიბლში, იქნება name რომელიც იქნება სტრინგი და აუცილებლად ჩასაწერი ველი ექნება
    name:{
        type:String, 
        required:true
    }
})

authorSchema.pre('remove', function(next){ 
    Book.find({author:this.id},(err,books)=>{
        if(err){
            next(err)
        }else if (books.length>0) //თუ ამ ავტორს აქვს წიგნები მაგ შემთხვევაში არ წაშლის ავტორს
        {
            next(new Error('This author has Books still'))
        }else{
            next()
        }
    })
})
module.exports = mongoose.model('Author', authorSchema) //შემდეგ ვაექსპორტებთ, ანუ ჩვენს თეიბლს ექნება სახელი Author ხოლო მეორე პარამეტრი იქნება ზემოთ აღწერილი სკიმა