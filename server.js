const express = require('express') //შემოგვაქვს ექსპრესი ჩვენს სერვერში
const app = express()
const expressLayouts = require('express-ejs-layouts') //ejs ის გამოყენებისთვის საჭიროა
const bodyParser = require('body-parser')
const methodOverride = require('method-override') //საჭიროა მეთოდის გადასაწერად, მაგალითად ფუთ და დელეთე მეთოდის გამოსაძახებლად
const flash = require('connect-flash') //საჭიროა გადამისამართების შემდეგ შეტყობინების გადასაცემად
const session = require('express-session') 
const passport = require('passport')

require('./config/passport')(passport); //config ფაილიდან შემოგვაქვს პასპორტი

//Get Routes
const indexRouter = require('./routes/index');//როუტეს ფოლდერსი არსებული როუტები შემოგვაქვს ჩვენს სერვერში
const authorRouter = require('./routes/authors');
const bookRouter = require('./routes/books');
const userRouter = require('./routes/user');

// Ejs and View
app.use(expressLayouts)
app.set('view engine', 'ejs') //რომ ნახოს ვიუ ფოლდერში არსებული ejs იგივე ჰტმლ ფაილები
app.set('views', __dirname +'/views') //ვუთითებთ სადაც არის ეჯს ფაილები
app.set('layout', 'layouts/layout') 
app.use(express.static('public'))
app.use(bodyParser.urlencoded({ limit:'300mb', extended:false}))
app.use(methodOverride('_method'))


//express session middlware
app.use(session({
    secret:'Secret',
    resave:true,
    saveUninitialized:true,
}))
//passport middlware
app.use(passport.initialize());
app.use(passport.session());

// Connect Flash გადამისამართების შემდეგ შეტყობინების გამოსატანად გვჭირდება ფლეში
app.use(flash())

//Global vars
app.use((req,res,next)=>{
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
})

//Connect To database
const mongoose = require('mongoose'); // დაკავშირება მონგოსთან
mongoose.connect('mongodb+srv://realnukri:WORKnukri13@cluster0.eg0ls.mongodb.net/bookDb?retryWrites=true&w=majority', {  maxPoolSize: 50, 
wtimeoutMS: 2500,
useNewUrlParser: true})
const db = mongoose.connection
db.on('error', error=>console.error(error))
db.once('open', () =>console.log('Connected to Mongoos'))

//Routes
app.use('/',indexRouter) //ზემოთ აღწერილ როუტებს ვუთითებთ როდესაც / როუტზე გადავა. 
app.use('/authors',authorRouter) 
app.use('/books',bookRouter) 
app.use('/user',userRouter) 

app.listen(process.env.PORT || 3000) //ვაძლევთ პორტს რასაც უნდა დაუკავშირდეს. 