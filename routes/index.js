const express = require('express')
const router = express.Router()
const Book = require('../models/book') 
const { ensureAuthenticated } =require('../config/auth'); //შემოგვაქვს კონფიგიდან ფუნქცია რომელიც უზრუნველყოფს დაადასტუროს არის თუარა ავტორიზებული მომხმარებელი

router.get('/', ensureAuthenticated, async (req,res)=>{
    let books 
    try{
        books = await Book.find().sort({createAt: 'desc'}).limit(10).exec()  // ვიღებ ბაზიდან წიგნებს, მაქსიმუმ 10 ცალს და დამატების დროის მიხედვით ვაწყობთ. 
        res.render('index', {books: books, user:req.user})
    }catch{
        books = []
    }
})

module.exports = router; 