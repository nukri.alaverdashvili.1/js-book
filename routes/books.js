const express = require('express')
const { route } = require('.')
const router = express.Router()
const Book = require('../models/book')
const Author = require('../models/author') //შემოგვაქვს მოდელები
const path = require('path') 
const fs = require('fs') //საჭიროა ფაილებტან სამუშაოდ
const imageMimeTypes = ['image/jpeg', 'image/jpg', 'image/png','images/gif'] // ფოტოების ფორმატებისთვის გვჭირდება

// All Books Route
router.get('/', async (req,res)=>{
    let query = Book.find() //რომ მოძებნოს სათაურის ან ატვირთვის თარიღის მიხედვით
    if(req.query.title != null && req.query.title != '')
    {
        query = query.regex('title', new RegExp(req.query.title, 'i')) // RegExp ეძებს ჩაწერილი ასოების მიხედვით და ფილტრავს //'i' ნიშნავს რომ დიდ და პატარა ასოებს არ აქვს მნიშვნელობა
    }
    if(req.query.publishedBefore != null && req.query.publishedBefore != '')
    {
        query = query.lte('publishDate',req.query.publishedBefore) // ნაკლებიას ნიშნავს lte
    }
    if(req.query.publishedAfter != null && req.query.publishedAfter != '') 
    {
        query = query.gte('publishDate',req.query.publishedAfter)  //მეტიას ნიშნავს gte
    }
    try{
        const books = await query.exec() // ქუერის მიხედვით გაფილტრავს
        res.render('books/index',{
            books:books,
            searchOptions: req.query
        })
    }catch{
        res.redirect('/')
    }
})

//New Book Route
router.get('/new', async (req,res)=>{
    renderNewPage(res, new Book(), 'new', false)
})

//Create Book Route
router.post('/', async (req,res)=>{
    const book = new Book({ // ახალი წიგნის დასამატებლად მოდელს ვიყენებთ და რექუესთიდან მიღებულ ინფორმაციას ვწერთ
        title:req.body.title,
        author:req.body.author,
        publishDate:new Date(req.body.publishDate),
        pageCount:req.body.pageCount, 
        description: req.body.description
    })
    saveCover(book, req.body.cover)

    try{
        const newBook = await book.save() // შენახვა მონაცემთა ბაზაში
        res.redirect(`books/${newBook.id}`)
    }catch{
        renderNewPage(res, book, 'new', true)
    }
})

//show Book Route 

router.get('/:id', async (req,res)=>{
    try{
        const book = await Book.findById(req.params.id).populate('author').exec() // populate აბრუნებს ავტორის ინფორმაციასაც როგორც რელეთიონშიფის.
        res.render('books/show',{book:book})
    }catch{
        res.redirect('/')
    }
})

//edit Book Route
router.get('/:id/edit', async (req,res)=>{

    try{
    const book = await Book.findById(req.params.id)
    renderNewPage(res, book, 'edit')
    }catch{
        res.redirect('/')
    }
})

// Update Book Route
router.put('/:id', async (req,res)=>{
    let book 
    try{
        book = await Book.findById(req.params.id)
        book.title = req.body.title
        book.author = req.body.author
        book.publishDate = new Date(req.body.publishDate)
        book.pageCount = req.body.pageCount
        book.description = req.body.description

        if(req.body.cover != null && req.body.cover !== '')
        {
            saveCover(book, req.body.cover)
        }
        await book.save()
        res.redirect(`/books/${book.id}`)
    }catch{
        if(book!= null){
            renderNewPage(res, book, 'new', true)
        }else{
            redirect('/')
        }
    }
})

// Delete book Page
router.delete('/:id', async (req,res)=>{
    let book 
    try{
        book = await Book.findById(req.params.id)
        await book.remove()
        res.redirect('/books')
    }catch{
        if(book != null){
            res.render('books/show',{
                book: book,
                errorMessage: 'Could not Remove Book'
            })
        }else{
            res.redirect('/')
        }
    }
})

async function renderNewPage(res, book, form, hasError){
    try{
        const authors = await Author.find({})
        const params = {
            authors: authors, 
            book: book
        }
        if(hasError)
        {
            if(form ==='edit'){
            params.errorMessage = 'Error Updating Book'
            }else{
            params.errorMessage = 'Error Creating Book'
            }
        }
        res.render(`books/${form}`, params)
    }catch{
        res.redirect('/books')
    }
}

function saveCover(book, coverEncoded) // coverEncoded არის ფაილი რომელიც ინფუთიდან მოდის ანუ ფაილი რომელიც აიტვირთება
{
    if(coverEncoded == null) {return} 
    const cover = JSON.parse(coverEncoded)
    if(cover != null && imageMimeTypes.includes(cover.type))
    {
        book.coverImage = new Buffer.from(cover.data, 'base64') // ვინახავთ როგორც ბაფერს
        book.coverImageType = cover.type //ვინახავს ამ იმიჯის ტაიპს ანუ jpg/png  თუ რომელიც არის
    }
}

module.exports = router;  