const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt') // საჭიროა პაროლის დაშიფრისთვის
const User = require('../models/user') 
const { body, validationResult } = require('express-validator');
const passport = require('passport')
require('../config/passport')(passport); //config ფაილიდან შემოგვაქვს პასპორტი
const { ensureNotAuthenticated } =require('../config/notauth'); //შემოგვაქვს კონფიგიდან ფუნქცია რომელიც უზრუნველყოფს დაადასტუროს არის თუარა ავტორიზებული მომხმარებელი
const { ensureAuthenticated } =require('../config/auth'); //შემოგვაქვს კონფიგიდან ფუნქცია რომელიც უზრუნველყოფს დაადასტუროს არის თუარა ავტორიზებული მომხმარებელი


//Register Form
router.get('/register', async (req,res)=>{
    res.render('auth/register');
});

//Register Process 
router.post('/register', body('email').isEmail(), body('password').isLength({min:6}), body('username').custom(value=>{ //თუ ბაზაში უკვე არსებობს იუზერი მაგ შემთხვევაში ერორის ფანჯარას გამოიტანს.
    return User.find({username:value}).then(user=>{
        if(user.length >0){
            return Promise.reject('Username already is use');
        }
    });
}),async (req,res)=>{
    const {name, email, username, password, password2}= req.body;
    const errors = validationResult(req); //ვინახავთ ყველა ერორს რომელიც გვაქვს
    if(!errors.isEmpty()){ //თუ ერორები ცარიელი არ არის გადავცემთ ამ ერორებს 
        let errorMessage =[] 
        errors.array().forEach(err=>{
             errorMessage.push(`${err.param} : ${err.msg}`)
            });
        res.render('auth/register', {error: errorMessage});
    }
    if(password !== password2){res.render('auth/register', {error: ['Confirm Password is worng']});}
    try{
        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(req.body.password, salt) // პაროლს დაშიფრავს 
        const user = new User({
            name,
            email,
            username,
            password:hashedPassword,
        })
        user.save();
        res.redirect('/user/login')
    }catch{
        res.status(500).send();
    }
})

router.get('/login', ensureNotAuthenticated, (req,res)=>{
    res.render('auth/login');
})

router.post('/login', (req,res, next)=>{ // ვამოწმებთ ნამდვილად არის თუარა იმეილი და პაროლის სიგრძე 6 ასო
    passport.authenticate('local',{
        successRedirect:'/',
        failureRedirect:'/user/login',
        failureFlash:true
    })(req,res,next);

})

router.get('/logout', ensureAuthenticated, (req,res)=>{
    req.logout();
    req.flash('success_msg','You are logged out')
    res.redirect('/user/login')
})

module.exports = router; 