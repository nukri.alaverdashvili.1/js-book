const express = require('express')
const { route } = require('.')
const router = express.Router()
const Author = require('../models/author')
const Book = require('../models/book')

// All Authors Route
router.get('/', async (req,res)=>{
    let searchOptions = {}
    if(req.query.name !=null && req.query.name !== ''){
        searchOptions.name = new RegExp(req.query.name, 'i')
    }
    try{
        const authors = await Author.find(searchOptions)
        res.render('authors/index',{ authors: authors, searchOptions: req.query})
    }catch{
        res.redirect('/')
    }
})

//New Authors Route
router.get('/new', (req,res)=>{
    res.render('authors/new', { author: new Author() })
})

//Create Author Route
router.post('/', async (req,res)=>{ //პოსტის სახლით გააგზავნის body.name ს ანუ რასაც ფორმა ნეიმში ჩავწერთ.
    const author = new Author({
        name:req.body.name
    })
    try{
        const newAuthor = await author.save()
         res.redirect(`authors/${newAuthor.id}`)
    }catch{
        res.render('authors/new',{
                    author: author,
                    errorMessage:'Error Create Author'
                    })
    }
})

router.get('/:id', async (req,res)=>{
    try{
        const author = await Author.findById(req.params.id)
        const books = await Book.find({author:author.id}).limit(6).exec()
        res.render('authors/show',{
            author: author,
            booksByAuthor: books
        })
    }catch(err){
        console.log(err)
        res.redirect('/')  
    }
    // res.send('Show Author' +req.params.id)   //params ში ის იწერება რაც ზემოთ ლინკში გვექნება დამატებული
})

router.get('/:id/edit', async (req,res)=>{
    try{
        const author = await Author.findById(req.params.id)
        res.render('authors/edit', { author: author})    //params ში ის იწერება რაც ზემოთ ლინკში გვექნება დამატებული
    }catch{
        res.redirect('/authors')
    }
})

router.put('/:id', async (req,res)=>{
    let author
    try{
        author = await Author.findById(req.params.id) // ვიპოვოთ ავტორუ აიდის მიხედვით
        author.name = req.body.name         // შევცვალოთ ავტორის სახელი ახალი მნიშვნელობით
        await author.save()  //გავაკეთოთ აფდეითი
         res.redirect(`/authors/${author.id}`)
    }catch{
        if (author ==null){ // თუ ავტორს ვერ იპოვის მთავარ გვერდზე გადამისამართდება
            res.redirect('/')
        }else{
            res.render('authors/edit',{
                        author: author,
                        errorMessage:'Error Updating Author'
                        })
        }
    }
})

router.delete('/:id',async (req,res)=>{
    let author
    try{
        author = await Author.findById(req.params.id) // ვიპოვოთ ავტორუ აიდის მიხედვით
        await author.remove()  //გავაკეთოთ აფდეითი
         res.redirect(`/authors`)
    }catch{
        if (author ==null){ // თუ ავტორს ვერ იპოვის მთავარ გვერდზე გადამისამართდება
            res.redirect('/')
        }else{
            res.redirect(`/authors/${author.id}`)
        }
    }
})

module.exports = router;  